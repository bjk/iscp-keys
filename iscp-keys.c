/*
    Copyright (C) 2014-2018 Ben Kibbey <bjk@luxsci.net>

    This file is part of iscp-keys.

    iscp-keys is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    iscp-keys is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with iscp-keys.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/input.h>
#include <err.h>
#include <string.h>
#include <signal.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pwd.h>
#include <ctype.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <netdb.h>
#include <regex.h>
#include <syslog.h>
#include <stdarg.h>
#include <time.h>

#ifdef HAVE_XOSD
#include <xosd.h>
#endif

#ifndef LINE_MAX
#define LINE_MAX 2048
#endif

#define VERSION_MAJOR 0
#define VERSION_MINOR 1

#define MAX_CMD_LEN  64		// for an ISCP command
#define MAX_CMDS     8		// comma separated list of ISCP commands per key
#define MAX_ZONES    4-1	// -1 since zone 1 is master
#define MAX_AUTOCMDS 64		// in the rcfile with MAX_CMDS per autocmd
#define MAX_VOLUME	78

struct key_s
{
  int code;
  int modifier;
  char **cmd;
  struct key_s *next;
} *keys;

struct zone_s
{
  int volume;		// current volume
  int offset;		// relative to Master volume
  char init;		// zone has been specified
  int mute;		// whether this zone is muted
} zone[MAX_ZONES];

struct autocmd_s
{
  regex_t re;
  char **cmd;
} autocmd[MAX_AUTOCMDS];

// ISCP header
struct header_s
{
  unsigned char magic[4];
  uint32_t size;
  uint32_t len;
  char version;
  char r1;
  char r2;
  char r3;
  unsigned char data[1];
} __attribute__ ((packed));

static int sockfd;
static int inputfd;
static char *device;
static int no_fork;
static unsigned master_volume;
static int update_zones;
static char **send_cmd;
static int do_autocmd;
static int quit;
static char *rcfile;
static int daemonized;
static int debug;
static char *log_file;
static char *pn;
static int modifier;
static int max_volume;
#ifdef HAVE_XOSD
static xosd *osd;
#endif

static int query_command (const char *cmd, char **result);

static void
log_write (const char *fmt, ...)
{
  va_list ap;
  char line[LINE_MAX];

  va_start (ap, fmt);
  vsnprintf (line, sizeof (line), fmt, ap);
  va_end (ap);

  if (!daemonized)
    {
      fprintf (stderr, "%s: %s\n", pn, line);
      return;
    }

  if (debug)
    {
      if (log_file)
        {
          FILE *fp = fopen (log_file, "a");

          if (fp)
            {
              struct tm *t;
              time_t now;
              char buf[255] = {0};

              time (&now);
              t = localtime (&now);
              strftime (buf, sizeof (buf), "%D %T", t);
              fprintf (fp, "%s %s\n", buf, line);
              fclose  (fp);
            }

          return;
        }

      if (daemonized)
        syslog (LOG_NOTICE, "%s", line);
    }
}

static void
cleanup (int reloading)
{
  struct key_s *k;
  int i;
  char **p;

  if (!reloading)
    {
      if (sockfd != -1)
        close (sockfd);

      if (inputfd != -1)
        close (inputfd);
    }

  for (k = keys; k;)
    {
      struct key_s *tmp = k->next;

      for (p = k->cmd; *p; p++)
	free (*p);

      free (k->cmd);
      free (k);
      k = tmp;
    }

  keys = NULL;

  for (i = 0; i < MAX_AUTOCMDS; i++)
    {
      if (autocmd[i].cmd)
	{
	  regfree (&autocmd[i].re);
	  for (p = autocmd[i].cmd; *p; p++)
	    free (*p);

	  free (autocmd[i].cmd);
          autocmd[i].cmd = NULL;
	}
    }
}

static void
catchsig (int sig)
{
  int status;

  switch (sig)
    {
    case SIGCHLD:
      waitpid (-1, &status, WNOHANG);
      break;
    case SIGTERM:
    case SIGINT:
      quit = 1;
      break;
    case SIGHUP:
      log_write ("reloading %s", rcfile);
      break;
    default:
      break;
    }
}

static int
run_command (const char *cmd)
{
  if (*cmd == '$')
    {
      switch (fork ())
        {
        case 0:
          system (cmd+1);
          _exit (0);
        case -1:
          log_write ("fork(): %s", strerror (errno));
          return -1;
        default:
          break;
        }

      return 0;
    }
  else
    return system (cmd);
}

static
int parse_internal_cmd (const char *cmd)
{
  char *tmp = strdup (cmd + 2), *p = tmp;

  if (p[strlen (p) - 1] == '\r')
    p[strlen (p) - 1] = 0;

  if (*p == 'Z' && isdigit (*(p+1)))
    {
      int z = atoi (++p) - 2;

      if (z >= sizeof (zone) / sizeof (zone[0])
          || z < 0)
        {
          free (tmp);
          return 1;
        }

      while (isdigit (*p))
        p++;

      if (strcmp (p, "VLUP") == 0)
        zone[z].offset++;
      else if (strcmp (p, "VLDOWN") == 0)
        zone[z].offset--;

      free (tmp);
      return 0;
    }

  free (tmp);
  return 1;
}

static int
parse_key (int code, char **result, int keysonly)
{
  struct key_s *k;

  for (k = keys; k; k = k->next)
    {
      if (k->code == code && modifier == k->modifier)
	{
	  int n;

	  for (n = 0; n < MAX_CMDS && k->cmd[n]; n++)
	    {
	      if (*k->cmd[n] == '!' && *(k->cmd[n]+1) != '9') // ISCP command
		{
                  int r;

                  if (keysonly)
                    continue;

		  r = query_command (k->cmd[n], result);
		  if (!r && result && *result && no_fork)
		    fprintf (stdout, "%s\n", *result);

		  if (result)
		    *result = NULL;
		  continue;
		}
                else if (*k->cmd[n] == '!' && *(k->cmd[n]+1) == '9')
                  {
                    parse_internal_cmd (k->cmd[n]);
                    continue;
                  }

              log_write ("exec: %s", k->cmd[n]);
              run_command (k->cmd[n]);
	    }

	  return 0;
	}
    }

  return 1;
}

static void
parse_val (char *p, int line, char **dst)
{
  while (*p && isspace (*p))
    p++;

  if (*p != '=')
    {
      log_write ("%s: %i: parse error", rcfile, line);
      return;
    }

  p++;

  while (*p && isspace (*p))
    p++;

  if (!*p)
    return;

  free (*dst);
  *dst = strdup (p);
}

static char **
parse_multi_cmd (char *str)
{
  char **p = NULL;
  char *s;
  int count = 0;

  while ((s = strsep (&str, ",")))
    {
      char **tmp;

      while (isspace (*s))
	s++;

      if (!*s)
	continue;

      tmp = realloc (p, (count + 2) * sizeof (char **));
      p = tmp;

      if (*s == '!')
	asprintf (&p[count++], "%s\r", s);
      else
	p[count++] = strdup (s);

      p[count] = NULL;
    }

  return p;
}

static int
parse_autocmd (char *str)
{
  char *p = strchr (str, ' ');
  int i;

  if (!p)
    return 1;

  p = strsep (&str, " ");
  for (i = 0; i < MAX_AUTOCMDS; i++)
    {
      int e;

      if (autocmd[i].cmd)
	continue;

      e = regcomp (&autocmd[i].re, p, REG_EXTENDED | REG_NOSUB);
      if (e)
	{
	  char buf[255];

	  regerror (e, &autocmd[i].re, buf, sizeof (buf));
	  return 1;
	}

      autocmd[i].cmd = parse_multi_cmd (str);
      break;
    }

  return 0;
}

static void
parse_rcfile (const char *filename)
{
  FILE *fp;
  struct passwd *pw = getpwuid (getuid ());
  char buf[LINE_MAX];
  char *p;
  int line = 0;

  if (!filename)
    snprintf (buf, sizeof (buf), "%s/.iscp-keysrc", pw->pw_dir);
  else
    snprintf (buf, sizeof (buf), "%s", filename);

  free (rcfile);
  rcfile = strdup (buf);
  fp = fopen (buf, "r");
  if (!fp)
    {
      warn ("%s", buf);
      return;
    }

  while ((p = fgets (buf, sizeof (buf), fp)))
    {
      int key;
      struct key_s *k;
      int mod = 0;

      if (*p == '#')
	{
	  line++;
	  continue;
	}

      if (p[strlen (p) - 1] == '\n')
	p[strlen (p) - 1] = 0;

      line++;

      while (*p && isspace (*p))
	p++;

      if (!*p)
	continue;

      if (!strncasecmp (p, "device", 6))
	{
	  p += 6;
	  parse_val (p, line, &device);
	  continue;
	}
      else if (!strncasecmp (p, "autocmd", 7))
	{
	  char *cmd = NULL;

	  p += 7;
	  parse_val (p, line, &cmd);
	  if (parse_autocmd (cmd))
	    log_write ("%s: %i: parse error", rcfile, line);

	  free (cmd);
	  continue;
	}

      switch (*p)
        {
          case '*':
            mod = KEY_LEFTCTRL;
            p++;
            break;
          case '&':
            mod = KEY_LEFTALT;
            p++;
            break;
          case '^':
            mod = KEY_LEFTSHIFT;
            p++;
            break;
        }

      /* The rest should be key code definitions. */
      if (!isdigit (*p))
	{
	  log_write ("%s: %i: parse error", rcfile, line);
	  continue;
	}

      key = atoi (p);

      while (*p && (isspace (*p) || isdigit (*p)))
	p++;

      if (*p != '=')
	{
	  log_write ("%s: %i: parse error", rcfile, line);
	  continue;
	}

      p++;

      while (*p && isspace (*p))
	p++;

      if (!*p)
	{
	  log_write ("%s: %i: parse error", rcfile, line);
	  continue;
	}

      k = malloc (sizeof (struct key_s));
      k->code = key;
      k->cmd = parse_multi_cmd (p);
      k->modifier = mod;
      k->next = keys;
      keys = k;
    }

  fclose (fp);
}

static int
connect_to_iscp (const char *host, unsigned port)
{
  struct addrinfo hints = { 0 };
  char portstr[6];
  struct addrinfo *addrs, *addr;

  if (daemonized)
    log_write ("connecting to %s:%d", host, port);

  hints.ai_socktype = SOCK_STREAM;
  snprintf (portstr, sizeof (portstr), "%u", port);
  int n = getaddrinfo (host, portstr, &hints, &addrs);
  if (n)
    {
      log_write ("%s", gai_strerror (n));
      return 1;
    }

  for (addr = addrs; addr; addr = addrs->ai_next)
    {
      sockfd = socket (addr->ai_family, SOCK_STREAM, 0);
      if (sockfd == -1)
	{
	  log_write ("%s", strerror (errno));
	  if (addr == addrs->ai_next)
	    break;
	  continue;
	}

      if (connect (sockfd, addr->ai_addr, sizeof (struct sockaddr)) == -1)
	{
	  log_write ("%s", strerror (errno));
	  close (sockfd);
	  sockfd = -1;
	  if (addr == addrs->ai_next)
	    return 1;

	  continue;
	}


      int v = 1;
      setsockopt (sockfd, IPPROTO_TCP, TCP_NODELAY, &v, sizeof (int));
      v = 1;
      setsockopt (sockfd, SOL_SOCKET, SO_KEEPALIVE, &v, sizeof (int));
      freeaddrinfo (addrs);
      return 0;
    }

  freeaddrinfo (addrs);
  return 1;
}

static void
init_header (void *hdr, const void *data, const size_t len)
{
  struct header_s *h = hdr;
  uint32_t n;

  h->magic[0] = 'I';
  h->magic[1] = 'S';
  h->magic[2] = 'C';
  h->magic[3] = 'P';
  n = 16;
  h->size = htonl (n);
  h->len = htonl (len);
  h->version = 0x01;
  h->r1 = 0x0;
  h->r2 = 0x0;
  h->r3 = 0x0;
  memcpy (h->data, data, len);
}

static void *
append_command (void *dst, size_t * len, const void *data, const size_t size)
{
  size_t n = (sizeof (struct header_s)-1) + size;

  n += sizeof(unsigned char) * (*len);
  void *p = realloc (dst, n);
  if (!p)
    return NULL;

  dst = p;
  init_header ((unsigned char *)dst + (*len), data, size);
  *len += n;
  return dst;
}

static ssize_t
do_write (const void *data, size_t len)
{
  fd_set fds;
  int n;

  FD_ZERO (&fds);
  FD_SET (sockfd, &fds);
  n = select (sockfd + 1, NULL, &fds, NULL, NULL);
  if (n > 0 && FD_ISSET (sockfd, &fds))
    return write (sockfd, data, len);

  return -1;
}

static int
send_command (const char *cmd)
{
  void *data = NULL;
  size_t len = 0;

  log_write ("ISCP [out]: %s", (char *)cmd);
  data = append_command (data, &len, cmd, strlen (cmd));
  len = do_write (data, len);
  free (data);
  return len > 0 ? 0 : 1;
}

#define VOL_PERCENT(n, max) (n*100/max)
static int
read_from_receiver (char **result)
{
  static char buf[255], *p;
  ssize_t len, rlen;
  struct header_s *hdr = malloc (sizeof (struct header_s) + MAX_CMD_LEN);
  int i;

  if (!send_cmd)
    fcntl (sockfd, F_SETFL, O_NONBLOCK);

  for (; !quit;)
    {
      len = recv (sockfd, hdr, sizeof (struct header_s) - 1, 0);
      if (len != sizeof (struct header_s) - 1)
	{
	  if (errno == EAGAIN || errno == EWOULDBLOCK)
	    {
	      *result = NULL;
	      break;
	    }
          else if (len == -1)
            log_write ("%s", strerror (errno));

	  log_write ("short read(): got %li, wanted %li", len,
		     (unsigned long) sizeof (struct header_s) - 1);
	  goto fail;
	}

      rlen = ntohl (hdr->len);
      if (rlen > MAX_CMD_LEN)
	{
	  *result = NULL;
	  break;
	}

      len = recv (sockfd, hdr->data, rlen, 0);
      if (len != rlen)
	{
	  if (errno == EAGAIN || errno == EWOULDBLOCK)
	    {
	      *result = NULL;
	      break;
	    }
          else if (len == -1)
            log_write ("%s", strerror (errno));

	  log_write ("short read(): got %li, wanted %li", len, rlen);
	  goto fail;
	}

      memcpy (buf, hdr->data, len);
      buf[len] = 0;
      p = strchr (buf, 0x1a);	// EOF character
      if (p)
	{
#ifdef HAVE_XOSD
          char str[255];
          unsigned v;
#endif

	  *p = 0;
          log_write ("ISCP [in]: %s", buf);

	  if (!strncmp (buf, "!1MVL", 5))
	    {
	      p = buf + 5;
	      master_volume = strtol (p, NULL, 16);

	      for (i = 0; i < MAX_ZONES; i++)
		{
		  if (zone[i].init)
		    {
		      zone[i].volume = master_volume + zone[i].offset;
                      if (!modifier)
                        update_zones = 1;
		    }
		}

#ifdef HAVE_XOSD
              snprintf (str, sizeof (str), "0x%s", buf+5);
              v = strtoul (str, NULL, 16);
              xosd_display (osd, 0, XOSD_printf, "Zone %u volume: %u", 1, v);
              xosd_display (osd, 1, XOSD_percentage,
                            VOL_PERCENT (master_volume, max_volume));
	    }
          else if (!strncmp (buf, "!1ZVL", 5) && zone[0].init) // zone 2
            {
              snprintf (str, sizeof (str), "0x%s", buf+5);
              v = strtoul (str, NULL, 16);
              xosd_display (osd, 2, XOSD_printf, "Zone %u volume: %u", 2, v);
              xosd_display (osd, 3, XOSD_percentage,
                            VOL_PERCENT (zone[0].volume, max_volume));
            }
          else if (!strncmp (buf, "!1VL", 4)) // zone N
            {
              unsigned z = buf[4] - '0';

              if (zone[z-2].init)
                {
                  snprintf (str, sizeof (str), "0x%s", buf+5);
                  v = strtoul (str, NULL, 16);
                  xosd_display (osd, (z*2)-1, XOSD_printf, "Zone %u volume: %u",
                                z, v);
                  xosd_display (osd, (z*2), XOSD_percentage,
                                VOL_PERCENT (zone[z-2].volume, max_volume));
                }
            }
          else if (!strncmp (buf, "!1AMT", 5)) // master zone mute
            {
              snprintf (str, sizeof (str), "0x%s", buf+5);
              v = strtoul (str, NULL, 16);
              if (v)
                {
                  xosd_display (osd, 0, XOSD_printf, "Zone 1 volume: MUTED");
                  xosd_display (osd, 1, XOSD_percentage, 0);
                }
              else
                {
                  v = master_volume;
                  xosd_display (osd, 0, XOSD_printf, "Zone %u volume: %u", 1, v);
                  xosd_display (osd, 1, XOSD_percentage,
                                VOL_PERCENT (master_volume, max_volume));
                }
            }
          else if (!strncmp (buf, "!1ZMT", 5)) // zone2 mute
            {
              snprintf (str, sizeof (str), "0x%s", buf+5);
              zone[0].mute = v = strtoul (str, NULL, 16);
              if (v)
                {
                  xosd_display (osd, 2, XOSD_printf, "Zone 2 volume: MUTED");
                  xosd_display (osd, 3, XOSD_percentage, 0);
                }
              else
                {
                  v = zone[0].volume;
                  xosd_display (osd, 2, XOSD_printf, "Zone %u volume: %u", 2, v);
                  xosd_display (osd, 3, XOSD_percentage,
                                VOL_PERCENT (zone[0].volume, max_volume));
                  update_zones = 1;
                }
            }
          else if (!strncmp (buf, "!1MT", 4)) // zone N mute
            {
              unsigned z = buf[4] - '0';

              snprintf (str, sizeof (str), "0x%s", buf+5);
              zone[z-2].mute = v = strtoul (str, NULL, 16);
              if (v)
                {
                  xosd_display (osd, (z*2)-1, XOSD_printf,
                                "Zone %u volume: MUTED", z);
                  xosd_display (osd, (z*2), XOSD_percentage, 0);
                }
              else
                {
                  v = zone[z-2].volume;
                  xosd_display (osd, (z*2)-1, XOSD_printf,
                                "Zone %u volume: %u", z, v);
                  xosd_display (osd, (z*2), XOSD_percentage,
                                VOL_PERCENT (zone[z-2].volume, max_volume));
                  update_zones = 1;
                }
            }
#else
        }
#endif

	  for (i = 0; i < MAX_AUTOCMDS; i++)
	    {
	      int n = 10;
	      regmatch_t pm[n];

	      if (!autocmd[i].cmd)
		continue;

	      if (regexec (&autocmd[i].re, buf, n - 1, pm, 0) != REG_NOMATCH)
		{
		  do_autocmd = i;
		  break;
		}
	    }
	}

      if (result)
	*result = buf;

      if (send_cmd)
	break;

      if (result && *result && no_fork)
	fprintf (stdout, "%s\n", *result);
    }

  free (hdr);
  fcntl (sockfd, F_SETFL, 0);
  return 0;

fail:
  free (hdr);
  *result = NULL;
  fcntl (sockfd, F_SETFL, 0);
  quit = 1;
  return 1;
}

static int
query_command (const char *cmd, char **result)
{
  int r = send_command (cmd);

  if (r)
    return r;

  if (result)
    *result = NULL;

  return read_from_receiver (result);
}

static void
usage (const char *prog, int ret)
{
  fprintf (ret == EXIT_SUCCESS ? stdout : stderr,
	   "Usage: %s [-hdv] [-m <volume>] [-f <filename] [-s <command>] [-n] [-k | -i] [<host> [<port>]]\n"
	   "    -f <filename> - use specified configuration file (~/.iscp-keysrc)\n"
	   "    -s <command>  - send command to receiver (can be used more than once)\n"
	   "    -n            - do not fork into background\n"
	   "    -2 <offset>   - zone 2 volume offset (relative to Master)\n"
	   "    -3 <offset>   - zone 3 volume offset (relative to Master)\n"
	   "    -4 <offset>   - zone 4 volume offset (relative to Master)\n"
           "    -k            - no iscp server, keyboard commands only\n"
           "    -i            - no keyboard device, iscp only\n"
	   "    -v            - version info\n"
           "    -d            - log debugging output\n"
           "    -l <filename> - log debugging info to filename\n"
           "    -m <volume>   - max volume level of the receiver (%u)\n"
	   "    -h            - this help text\n", prog, MAX_VOLUME);

  exit (ret);
}

/* Send zone volumes in multiple packets per write(). Seems to fix AV receiver
 * skipping some reads. */
static void
update_zone_volume ()
{
  void *data = NULL;
  size_t len = 0;
  int i;

  for (i = 0; i < MAX_ZONES; i++)
    {
      if (zone[i].init && !zone[i].mute)
	{
	  char buf[16];

	  if (!i)
	    snprintf (buf, sizeof (buf), "!1ZVL%02X\r",
		      zone[i].volume <= 0 ? 0 : zone[i].volume);
	  else
	    snprintf (buf, sizeof (buf), "!1VL%i%02X\r", i + 2,
		      zone[i].volume <= 0 ? 0 : zone[i].volume);

          log_write ("ISCP [out]: %s", buf);
	  data = append_command (data, &len, buf, strlen (buf));
	}
    }

  update_zones = 0;
  (void) do_write (data, len);
  free (data);
}

static void
send_autocmd ()
{
  char **p;
  void *data = NULL;
  size_t len = 0;
  int cmd = do_autocmd;

  do_autocmd = -1;
  log_write ("doing autocmd's ...");

  for (p = autocmd[cmd].cmd; *p; p++)
    {
      if (*(*p) == '!')		// ISCP command
	{
          void *tmp = append_command (data, &len, *p, strlen (*p));

	  if (!tmp)
	    {
	      free (data);
	      return;
	    }

	  data = tmp;
          log_write ("ISCP [out]: %s", *p);
	}
      else
	{
          log_write ("exec: %s", *p);
          run_command (*p);
	}
    }

  if (data)
    {
      (void) do_write (data, len);
      free (data);
    }

  log_write ("end autocmd's");
}

int
main (int argc, char **argv)
{
  int opt;
  int send_idx = 0;
  int port = 60128;
  char *result, *filename = NULL;
  int i;
  int keysonly = 0;
  int iscponly = 0;
  int zones = 1;

  inputfd = sockfd = -1;
  do_autocmd = -1;
  pn = argv[0];
  max_volume = MAX_VOLUME;

  while ((opt = getopt (argc, argv, "l:m:ikd2:3:4:hvf:ns:")) != -1)
    {
      switch (opt)
	{
        case 'l':
          log_file = optarg;
          break;
        case 'm':
          max_volume = atoi (optarg);
          break;
        case 'i':
          iscponly = 1;
          break;
        case 'k':
          keysonly = 1;
          break;
        case 'd':
          debug++;
          break;
	case '2':
	case '3':
	case '4':
	  zone[opt - '0' - 2].offset = atoi (optarg);
	  zone[opt - '0' - 2].init = 1;
          zones++;
	  break;
	case 'f':
	  filename = optarg;
	  break;
	case 's':
	  {
	    char **tmp = realloc (send_cmd,
				  (send_idx + 2) * sizeof (char **));
	    send_cmd = tmp;
	  }

	  asprintf (&send_cmd[send_idx], "%s\r", optarg);
	  send_cmd[++send_idx] = NULL;
	  break;
	case 'n':
	  no_fork = 1;
	  break;
	case 'v':
	  printf ("iscp-keys v%i.%i\n", VERSION_MAJOR, VERSION_MINOR);
	  exit (EXIT_SUCCESS);
	case 'h':
	  usage (argv[0], EXIT_SUCCESS);
	default:
	  usage (argv[0], EXIT_FAILURE);
	}
    }

  if (argc - optind == 2)
    port = atoi (argv[optind + 1]);
  else if ((argc - optind > 2 || argc - optind == 0) && !keysonly)
    usage (argv[0], EXIT_FAILURE);
  else if (keysonly && iscponly)
    usage (argv[0], EXIT_FAILURE);

  parse_rcfile (filename);
  if (!iscponly && device)
    {
      char buf[255];

      snprintf (buf, sizeof (buf), "%s/%s", DEV_ROOT_PATH, device);
      inputfd = open (buf, O_RDONLY);

      if (inputfd == -1)
	err (EXIT_FAILURE, "%s", buf);
    }
  else if (no_fork)
    inputfd = STDIN_FILENO;

  if (!device && !iscponly && keysonly)
    errx (EXIT_FAILURE, "No device and -k specified. Exiting.");

  if (!keysonly && connect_to_iscp (argv[optind], port))
    exit (EXIT_FAILURE);

  if (!keysonly && send_cmd)
    {
      no_fork = 1;
      for (i = 0; send_cmd[i]; i++)
	{
	  if (query_command (send_cmd[i], &result))
	    exit (EXIT_FAILURE);

	  if (result)
	    fprintf (stdout, "%s\n", result);
	}

      exit (EXIT_SUCCESS);
    }

  // Need to do this before anything else. Seems the file descriptors arent
  // getting setup correctly otherwise.
  if (!no_fork)
    {
      if (daemon (0, 0) == -1)
	err (EXIT_FAILURE, "%s", strerror (errno));

      daemonized = 1;
      openlog ("iscp-keys", LOG_NDELAY | LOG_PID, LOG_DAEMON);
      log_write ("version %d.%d starting", VERSION_MAJOR, VERSION_MINOR);
    }

  signal (SIGCHLD, catchsig);
  signal (SIGTERM, catchsig);
  signal (SIGINT, catchsig);
  signal (SIGHUP, catchsig);

#ifdef HAVE_XOSD
  osd = xosd_create (zones*2);
  xosd_set_timeout (osd, 3);
  xosd_set_align (osd, XOSD_center);
  xosd_set_pos (osd, XOSD_bottom);
  xosd_set_font (osd, "-*-lucida-*-r-*-*-22-*-*-*-*-*-*-*");
  xosd_set_vertical_offset (osd, 20);
  xosd_set_colour (osd, "LightGreen");
#endif

  for (i = 0; !keysonly && i < MAX_ZONES; i++)
    {
      if (zone[i].init)
	{
	  (void) send_command ("!1MVLQSTN\r");
	  break;
	}
    }

  while (!quit)
    {
      fd_set fds;
      int n;

      result = NULL;

      FD_ZERO (&fds);

      if (inputfd != -1)
	FD_SET (inputfd, &fds);

      if (sockfd != -1)
        FD_SET (sockfd, &fds);

      n = inputfd == -1 ? sockfd : sockfd > inputfd ? sockfd : inputfd;
      n = select (n + 1, &fds, NULL, NULL, NULL);
      if (n > 0)
	{
	  if (sockfd != -1 && FD_ISSET (sockfd, &fds))
	    n = read_from_receiver (&result);
	  else if (inputfd != -1 && FD_ISSET (inputfd, &fds))
	    {
              if (!iscponly)
                {
                  struct input_event e;
                  ssize_t len = read (inputfd, &e, sizeof (e));

                  if (len == -1 || len == 0)
                    {
                      log_write ("read error");
                      break;
                    }

                  if (len != sizeof (e))
                    {
                      usleep (50000);
                      continue;
                    }

                  if (e.type == EV_KEY)
                    {
                      switch (e.code)
                        {
                        case KEY_LEFTSHIFT:
                        case KEY_RIGHTSHIFT:
                          e.code = KEY_LEFTSHIFT;
                          modifier = e.value ? e.code : 0;
                          continue;
                        case KEY_LEFTALT:
                        case KEY_RIGHTALT:
                          e.code = KEY_LEFTALT;
                          modifier = e.value ? e.code : 0;
                          continue;
                        case KEY_LEFTCTRL:
                        case KEY_RIGHTCTRL:
                          e.code = KEY_LEFTCTRL;
                          modifier = e.value ? e.code : 0;
                          continue;
                        default:
                          break;
                        }
                    }

                  if (e.type != EV_KEY || !e.value)
                    continue;

                  n = parse_key (e.code, &result, keysonly);
                }
              else
                {
                  char buf[255] = {0};
                  ssize_t len = read (inputfd, buf, sizeof (buf));

                  if (len == -1 || len == 0)
                    {
                      log_write ("%s", len == 0 ? "eof" : "read error");
                      break;
                    }

                  if (buf[len-1] == '\n')
                    buf[len-1] = '\r';

                  n = query_command (buf, &result);
                }
            }

          if (!keysonly && !n && update_zones)
            update_zone_volume ();
          else if (n)
            {
              do_autocmd = -1;
              usleep (50000);
            }

          if (do_autocmd != -1)
            send_autocmd ();
	}
      else if (n == -1)
	{
	  if (errno != EINTR)
            {
              log_write ("%s", strerror (errno));
              break;
            }
          else
            {
              cleanup (1);
              parse_rcfile (rcfile);
            }
	}
    }

  cleanup (0);
  free (rcfile);
  free (device);
#ifdef HAVE_XOSD
  xosd_destroy (osd);
#endif
  log_write ("exiting");
  closelog ();
  exit (EXIT_SUCCESS);
}
